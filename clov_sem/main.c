#include "main.h"

void posliKomentar(int oznacenieHraca) {
    printf("***************************************************\n");
    printf("Chces pridat komentar? (ano=1/nie=0)\n");
    printf("***************************************************\n");
    int ans;
    scanf("%d", &ans);
    if(ans == 1) {
        char _komentar[50];
        printf("Zadaj komentar: ");
        //scanf("%s", &_komentar);
        char temp;
        //vycistenie buffera
        scanf("%c", &temp);
        //citanie komentara
        scanf("%[^\n]", &_komentar);

        vymazPole(komentar, 65);
        strcat(komentar, dajFarbuHraca(oznacenieHraca));
        strcat(komentar, " hrac -> ");
        strcat(komentar, _komentar);
        // Posle komentar
        !oznacenieHraca ? posliSpravu(SERVER, CHAT_IDENTIFIKATOR) : posliSpravu(KLIENT, CHAT_IDENTIFIKATOR);
    }
}

void vymazPole(char *pole, int velkost) {
    for (int i = 0; i < velkost; i++) {
        pole[i] = '\0';
    }
}

void *spustenieHry(void *args) {
    SPUSTANIE *data = (SPUSTANIE *) args;
    while (*data->spustenieHry == false) {
        int volba;
        printf("Aktualne pripojenych hracov = %d\n", pocetHracov);
        printf("Spustit hru?\n");
        printf("1.. Spustit hru\n");
        scanf("%d", &volba);
        if (volba == 1) {
            pthread_mutex_lock(data->mutexPripajanie);
            (*data->spustenieHry) = true;
            pthread_mutex_unlock(data->mutexPripajanie);
        }
    }
    sleep(1);
    return NULL;
}

void *skusPripojit(void *args) {
    PRIPAJANIE *data = (PRIPAJANIE *) args;
    while (*data->spustenieHry == false) {
        pthread_mutex_lock(data->mutexPripajanie);
        fd_set set;
        struct timeval timeout;
        int rv = -1;
        FD_ZERO(&set);
        FD_SET(sockfd, &set);
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;

        rv = select(sockfd + 1, &set, NULL, NULL, &timeout);
        if (rv == -1) {
            printf("error so selectom\n");
        } else if (rv == 0) {
            //printf("cas na overovanie ci sa niekto chce pripojit vyprsal(1 sec) \n");
        }
            //niekto sa chce pripojit
        else {
            printf("Pripaja sa hrac\n");
            newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &cli_len);
            if (newsockfd < 0) {
                perror("ERROR on accept");
                //return 3;
            }
            hraci[pocetHracov].socketHraca = newsockfd;
            pocetHracov++;

            bzero(buffer, 256);
            n = read(newsockfd, buffer, 255);
            if (n < 0) {
                perror("Error reading from socket");
                //return 4;
            }
            printf("Pripojil sa hrac %s\n", buffer);

            //posielanie kolky hrac je
            char msg[5];
            sprintf(msg, "%d", pocetHracov);
            n = write(newsockfd, msg, strlen(msg) + 1);

            if (n < 0) {
                perror("Error writing to socket");
                //return 5;
            }
            printf("aktualny pocet hracov = %d\n", pocetHracov);
            printf("Spustit hru?\n");
            printf("1.. Spustit hru\n");
        }
        if (pocetHracov == 4) {
            printf("Dosiahnuty max pocet hracov\n");
            pthread_mutex_unlock(data->mutexPripajanie);
            break;
        }
        pthread_mutex_unlock(data->mutexPripajanie);
        //bez sleepu nedokazalo druhe vlakno vcas zareagovat na odomknutie
        sleep(1);
    }
    return NULL;
}

//vrati 1 ked vykonalo okej a -1 ked sa nemohlo vykonat
int vykonaj(char *pSprava) {
    const char oddelovac[2] = " ";
    char *token;
    token = strtok(pSprava, oddelovac);
    //najprv je oznacenie typu prijatej spravy
    const char *typSpravy = token;
    //printf("Typ spravy = %s\n", token);
    const char *hodnotaSpravy;
    token = (atoi(typSpravy) == CHAT_IDENTIFIKATOR ? strtok(NULL, "") : strtok(NULL, oddelovac));
    //potom hodnota pre spravu
    hodnotaSpravy = token;

    switch (atoi(typSpravy)) {
        case POCET_HRACOV_IDENTIFIKATOR:
            pocetHracov = atoi(hodnotaSpravy);
            break;
        case HRAC_NA_RADE_IDENTIFIKATOR:
            if (hracNaRade != NEDEFINOVANE) {
                //pokial sa manipuluje s hracom na rade
                return -1;
            }
            hracNaRade = atoi(hodnotaSpravy);
            printf("\nNa rade je hrac = %s\n", dajFarbuHraca(hracNaRade));
            break;
        case HOD_HRACA_IDENTIFIKATOR:
            if (hod != NEDEFINOVANE) {
                //pokial sa manipuluje s hodom kockou
                return -1;
            }
            hod = atoi(hodnotaSpravy);
            printf("Zistena hodnota hodu hraca = %d\n", hod);
            break;
        case POHYB_FIGURKOU_IDENTIFIKATOR:
            if (aktualnaFigurka != NEDEFINOVANE) {
                //pokial sa manipuluje s figurkou
                return -1;
            }
            aktualnaFigurka = atoi(hodnotaSpravy);
            printf("Zistene ktorou figurkou chce pohnut = %d\n", aktualnaFigurka);
            break;
        case VYHERCA_IDENTIFIKATOR:
            //konci sa hra
            koniec = true;
            printf("Zisteny vyherca \n");
            break;
        case CHAT_IDENTIFIKATOR:
            if (server) {
                strcpy(komentar, hodnotaSpravy);
                posliSpravu(SERVER, CHAT_IDENTIFIKATOR);
                printf("||||||||CHAT: %s ||||||||\n", hodnotaSpravy);
            } else {
                printf("||||||||CHAT: %s ||||||||\n", hodnotaSpravy);
            }
            break;
        case KONIEC_HRY_IDENTIFIKATOR:
            if (!server){
                //posiela serveru ze obdrzal spravu o ukonceni nech sa zrusi vlakno pre citanie sprav od klienta
                posliSpravu(KLIENT, KONIEC_HRY_IDENTIFIKATOR);
                printf("Hra bola ukoncena serverom\n");
                koniec = true;
            }
            break;
    }
    return 1;
}

void *hraj(void *args) {
    //printf("Zacina vlakno hraj\n");
    HRA *data = (HRA *) args;
    int hracNaRadePom = NEDEFINOVANE;
    hracNaRade = NEDEFINOVANE;
    hod = NEDEFINOVANE;
    aktualnaFigurka = NEDEFINOVANE;
    while (!koniec) {
        hracNaRadePom++;
        hracNaRadePom = hracNaRadePom % pocetHracov;
        do {
            if (hod == 6) {
                hod = NEDEFINOVANE;
            }
            hracNaRade = hracNaRadePom;
            vykresleniePolicok();
            printf("\nNa rade je hrac = %s\n", dajFarbuHraca(hracNaRade));
            sleep(1);
            //odosle klientom spravu o hracovi na rade
            posliSpravu(SERVER, HRAC_NA_RADE_IDENTIFIKATOR);
            //ak je na rade serverovy hrac
            if (hracNaRade == 0) {
                // bude hadazat server
                hod = hodKockou();
                //hod = 6;
                printf("----------Si na rade!----------\n");
                printf("Na kocke si hodil %d\n", hod);
                sleep(1);
                //rozposielanie klientom kolko hodil
                posliSpravu(SERVER, HOD_HRACA_IDENTIFIKATOR);
                /*
                if (jeMoznyPohybNejakouFigurkou() == true) {
                    printf("Pohyb fugurkou je mozny.\n");
                } else {
                    printf("Nie je mozny ziadny pohyb figurkou!\n");
                }
                 */
                printf("Ktorou figurkou chces pohnut(1,2,3,4)?\n");
                scanf("%d", &aktualnaFigurka);

                //rozposielanie klientom ktorou figurkou chce pohnut
                posliSpravu(SERVER, POHYB_FIGURKOU_IDENTIFIKATOR);

                int aktualnePolicko = pohniFigurkou(aktualnaFigurka);
                if (aktualnePolicko != -1) {
                    printf("---------------------------------------------------\n");
                    printf("Figurka %d hraca %s sa pohla na policko %d \n", aktualnaFigurka,
                           dajFarbuHraca(hracNaRade),
                           aktualnePolicko);
                    printf("---------------------------------------------------\n");
                } else {
                    printf("---------------------------------------------------\n");
                    printf("Figurka sa nemohla pohnut\n");
                    printf("---------------------------------------------------\n");
                }
                int vitaz = zistiVitaza();
                if (vitaz != -1) {
                    printf("-----------------Vitazi %s hrac!-------------------\n", dajFarbuHraca(vitaz));
                    koniec = true;
                    pthread_cond_signal(data->condCitajPrijatuSpravu);
                    break;
                }
            } else {
                // ak je na rade ktorykolvek klient, hrac na serveri moze poslat spravu
                posliKomentar(0);
                //ak je na rade klient
                //musi overit ci zistil hod(ak je -1 tak este neprecital tak bude cakat)
                //printf("Hraj sa ide locknut pred zistovanim hodu\n");
                pthread_mutex_lock(data->mutexHra);
                while (hod == NEDEFINOVANE) {
                    //printf("Hod este nebol precitany od klienta\n");
                    pthread_cond_wait(data->condHraj, data->mutexHra);
                }
                //ked uz vieme hod tak posleme ostatnym hracom
                posliSpravu(SERVER, HOD_HRACA_IDENTIFIKATOR);

                //musi overit ci zistil pohyb figurky(ak je -1 tak este neprecital tak bude cakat)
                while (aktualnaFigurka == NEDEFINOVANE) {
                    //printf("Aktualna figurka este nebola precitana od klienta\n");
                    pthread_cond_wait(data->condHraj, data->mutexHra);
                }
                //ked uz vieme ktorou figurkou chce pohnut tak posleme ostatnym hracom
                posliSpravu(SERVER, POHYB_FIGURKOU_IDENTIFIKATOR);

                int aktualnePolicko = pohniFigurkou(aktualnaFigurka);
                pthread_mutex_unlock(data->mutexHra);
                pthread_cond_signal(data->condVykonajSpravu);
                //printf("Hraj sa unlockol po pohybe figurky\n");
                if (aktualnePolicko != -1) {
                    printf("---------------------------------------------------\n");

                    printf("Figurka %d hraca %s sa pohla na policko %d \n", aktualnaFigurka,
                           dajFarbuHraca(hracNaRade),
                           aktualnePolicko);
                    printf("---------------------------------------------------\n");
                } else {
                    printf("---------------------------------------------------\n");
                    printf("Figurka sa nemohla pohnut\n");
                    printf("---------------------------------------------------\n");
                }
                int vitaz = zistiVitaza();
                if (vitaz != -1) {
                    printf("-----------------Vitazi %s hrac!-------------------\n", dajFarbuHraca(vitaz));
                    koniec = true;
                    pthread_cond_signal(data->condCitajPrijatuSpravu);
                    break;
                }
            }
            //koniec tahu
            hracNaRade = NEDEFINOVANE;
            if (hod != 6) {
                hod = NEDEFINOVANE;
            }
            aktualnaFigurka = NEDEFINOVANE;
        } while (hod == 6);

        if (!koniec) {
            printf("-------------------------------------\n");
            printf("Vypnut hru?(ak ano tak 0)\n");
            printf("-------------------------------------\n");
            scanf("%d", &volba);
            printf("zadane = %d\n", volba);
            if (volba == 0) {
                koniec = true;
                pthread_cond_signal(data->condCitajPrijatuSpravu);
                //posle klientom ze ukoncil hru
                posliSpravu(SERVER,KONIEC_HRY_IDENTIFIKATOR);
            }
        }
    }
    //printf("Konci vlakno hraj\n");
    return NULL;
}

void *vykonajSpravu(void *args) {
    //printf("Zacina vlakno vykonaj spravu\n");
    VYK_SPRAV *data = (VYK_SPRAV *) args;
    while (!koniec) {
        //printf("vykonaj spravu sa ide locknut\n");
        pthread_mutex_lock(data->mutexClovece);
        //printf("vykonaj spravu sa lockol\n");
        while (*data->pocetNacitanych == 0) {
            if (koniec){
                pthread_mutex_unlock(data->mutexClovece);
                pthread_cond_signal(data->condHraj);
                break;
            }
            //printf("Nie je co vykonavat. Cakam na spravy\n");
            pthread_cond_wait(data->condCitajPrijatuSpravu, data->mutexClovece);
        }
        if (koniec){
            pthread_mutex_unlock(data->mutexClovece);
            pthread_cond_signal(data->condHraj);
            break;
        }
        //printf("Pocet nacitanych sprav = %d\n", *data->pocetNacitanych);
        char *sprava = prijateSpravy[(*data->pocetNacitanych) - 1];
        //printf("sprava %s sa ide vykonat\n", sprava);
        int vykonane = -1;
        //printf("vykonaj spravu sa ide locknut pre hraj\n");
        pthread_mutex_lock(data->mutexHra);
        //printf("vykonaj spravu sa lockol pre hraj\n");
        while (vykonane == -1) {
            vykonane = vykonaj(sprava);
            if (vykonane == -1) {
                //printf("Nemohla sa vykonat sprava\n");
                pthread_cond_wait(data->condVykonajSpravu, data->mutexHra);
            };
        }
        pthread_mutex_unlock(data->mutexHra);
        pthread_cond_signal(data->condHraj);
        //printf("vykonaj spravu sa unlockol pre hraj\n");
        (*data->pocetNacitanych)--;
        pthread_mutex_unlock(data->mutexClovece);
        //printf("vykonaj spravu sa unlockol\n");
        //ak budem chciet napriklad ze znova nech nacitava az ked budu vsetky precitane tak dam podmienku
        pthread_cond_signal(data->condNacitajSpravu);
        sleep(1);
    }
    pthread_cond_signal(data->condHraj);
    //printf("Konci vlakno vykonaj spravu\n");
    return NULL;
}

void *citajSpravy(void *args) {
    //printf("Zacina vlakno citaj spravy\n");
    CIT_SPRAV *data = (CIT_SPRAV *) args;
    while (!koniec) {
        bzero(buffer, 256);
        //printf("---Ide sa pokusit precitat spravu. socket = %d\n",data->socket);
        n = read(data->socket, buffer, 255);
        if (n <= 0){
            //printf("Neda sa citat\n");
            if (koniec){
                break;
            }
        }
        //printf("Sprava prevzata\n");
        //printf("hodnota spravy = %s\n", buffer);
        const char oddelovac[2] = "$";
        char *token;
        token = strtok(buffer, oddelovac);
        if (koniec){
            break;
        }
        pthread_mutex_lock(data->mutexClovece);
        while (token != NULL) {
            //printf("hodnota tokenu = %s\n", token);
            //        pthread_mutex_lock(data->mutexClovece);
            //printf("citac sprav sa lockol\n");
            while (*data->pocetNacitanych == 10) {
                printf("Cakam kym sa uvolni miesto na spravy\n");
                pthread_cond_wait(data->condNacitajSpravu, data->mutexClovece);
            }
            //pridanie spravy do nacitanych sprav
            strcpy(prijateSpravy[*data->pocetNacitanych], token);
            //printf("token %s bol pridany do prijatych sprav na index %d\n", token, *data->pocetNacitanych);
            //printf("sprava na indexe %d = %s\n", *data->pocetNacitanych, prijateSpravy[*data->pocetNacitanych]);
            (*data->pocetNacitanych)++;
            //printf("Pocet nacitanych sprav = %d\n", *data->pocetNacitanych);
            token = strtok(NULL, oddelovac);
            if (token == NULL) {
                pthread_mutex_unlock(data->mutexClovece);
                pthread_cond_signal(data->condCitajPrijatuSpravu);
                //printf("citac sprav sa unlockol\n");
            }
        }
    }
    //printf("Konci vlakno citaj spravy\n");
    return NULL;
}

void *klientHra(void *args){
    //printf("Zacina vlakno hraj\n");
    HRA_KLIENT *data = (HRA_KLIENT *) args;
    hracNaRade = NEDEFINOVANE;
    hod = NEDEFINOVANE;
    aktualnaFigurka = NEDEFINOVANE;
    while (!koniec){
        vykresleniePolicok();
        //zistenie kto je na rade
        pthread_mutex_lock(data->mutexHra);
        while (hracNaRade == NEDEFINOVANE){
            if (koniec){
                pthread_mutex_unlock(data->mutexHra);
                break;
            }
            posliKomentar(data->oznacenieHraca);
            //printf("Hrac na rade este nebol zisteny\n");
            pthread_cond_wait(data->condHraj, data->mutexHra);
        }
        if (hracNaRade == data->oznacenieHraca){
            //klient je na rade
            printf("\n----------Si na rade----------\n");
            hod = hodKockou();
            printf("Na kocke si hodil %d\n", hod);

            //poslanie serveru moj hod
            //sleep(1);
            posliSpravu(KLIENT, HOD_HRACA_IDENTIFIKATOR);

            /*
            if (jeMoznyPohybNejakouFigurkou() == true) {
                printf("Pohyb fugurkou je mozny.\n");
            } else {
                printf("Nie je mozny ziadny pohyb figurkou!\n");
            }
            */
            printf("Ktorou figurkou chces pohnut(1,2,3,4)?\n");
            scanf("%d", &aktualnaFigurka);

            //poslanie serveru ktorou figurkou chcem pohnut
            posliSpravu(KLIENT, POHYB_FIGURKOU_IDENTIFIKATOR);

            int aktualnePolicko = pohniFigurkou(aktualnaFigurka);
            if (aktualnePolicko != -1) {
                printf("---------------------------------------------------\n");
                printf("figurka %d hraca %s sa pohla na policko %d \n", aktualnaFigurka,
                       dajFarbuHraca(hracNaRade),
                       aktualnePolicko);
                printf("---------------------------------------------------\n");
            } else {
                printf("---------------------------------------------------\n");
                printf("Figurka sa nemohla pohnut\n");
                printf("---------------------------------------------------\n");
            }
            int vitaz = zistiVitaza();
            if (vitaz != -1) {
                printf("-----------------Vitazi %s hrac!-------------------\n", dajFarbuHraca(vitaz));
                koniec = true;
                pthread_cond_signal(data->condCitajPrijatuSpravu);
                posliSpravu(KLIENT, KONIEC_HRY_IDENTIFIKATOR);
            }
        } else{
            //na rade je niekto iny ako ja
            //zistuje kolko hodil
            while (hod == NEDEFINOVANE) {
                if (koniec){
                    pthread_mutex_unlock(data->mutexHra);
                    break;
                }
                //printf("Hod este nebol precitany zo serveru\n");
                pthread_cond_wait(data->condHraj, data->mutexHra);
            }
            //zistuje ktorou figurkou chce pohnut
            while (aktualnaFigurka == NEDEFINOVANE) {
                if (koniec){
                    pthread_mutex_unlock(data->mutexHra);
                    break;
                }
                //printf("Aktualna figurka este nebola precitana od klienta\n");
                pthread_cond_wait(data->condHraj, data->mutexHra);
            }
            //sleep(1);
            //pohne figurkou tak ako pohol hrac cez siet
            int aktualnePolicko = pohniFigurkou(aktualnaFigurka);
            if (aktualnePolicko != -1) {
                printf("---------------------------------------------------\n");
                printf("figurka %d hraca %s sa pohla na policko %d \n", aktualnaFigurka,
                       dajFarbuHraca(hracNaRade),
                       aktualnePolicko);
                printf("---------------------------------------------------\n");
            } else {
                printf("---------------------------------------------------\n");
                printf("Figurka sa nemohla pohnut\n");
                printf("---------------------------------------------------\n");
            }
            int vitaz = zistiVitaza();
            if (vitaz != -1) {
                printf("-----------------Vitazi %s hrac!-------------------\n", dajFarbuHraca(vitaz));
                koniec = true;
                pthread_cond_signal(data->condCitajPrijatuSpravu);
                posliSpravu(KLIENT, KONIEC_HRY_IDENTIFIKATOR);
            }
        }
        hracNaRade = NEDEFINOVANE;
        hod = NEDEFINOVANE;
        aktualnaFigurka = NEDEFINOVANE;
        pthread_mutex_unlock(data->mutexHra);
        pthread_cond_signal(data->condVykonajSpravu);
        sleep(1);
    }
    //printf("Konci vlakno hraj\n");
    return NULL;
}

//argumenty budu specifikovane nasledovne
//1. argument 0/1 ci je server alebo klient
//2. argument host adresa
//3. argument port
int main(int argc, char *argv[]) {
    bool zapisujeSa = true;
    bool citaSa = false;
    srand(time(0));
    for (int i = 0; i < 40; i++) {
        hraciePolia[i][0] = -1;
        hraciePolia[i][1] = -1;
    }
    for (int i = 0; i < 16; i++) {
        finalnePolia[i] = -1;
        zaciatocnePolia[i] = -1;
    }
    //v argumentoch sa pri spustani specifikuje ci chceme pustat klienta alebo server
    if (atoi(argv[1]) == SERVER) {
        //serverova cast
        server = true;

        //ak nie je zadany v argumentoch port cez ktory sa ma komunikovat
        if (argc < 4) {
            fprintf(stderr, "usage %s port\n", argv[0]);
            return 1;
        }

        bzero((char *) &serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = INADDR_ANY;
        serv_addr.sin_port = htons(atoi(argv[3]));

        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0) {
            perror("Error creating socket");
            return 1;
        }

        if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
            perror("Error binding socket address");
            return 2;
        }

        printf("Lobby vytvorene\n");

        listen(sockfd, 5);
        cli_len = sizeof(cli_addr);

        hraci[0].socketHraca = -1;

        //pripajanie hracov a spustanie hry cez vlakna
        bool spustiHru = false;

        pthread_t vlaknoSpustanie;
        pthread_t vlaknoPripajanie;

        pthread_mutex_t mutexPripajanie;
        pthread_mutex_init(&mutexPripajanie, NULL);


        SPUSTANIE spustanieData;
        spustanieData.mutexPripajanie = &mutexPripajanie;
        spustanieData.spustenieHry = &spustiHru;
        PRIPAJANIE pripajanieData;
        pripajanieData.mutexPripajanie = &mutexPripajanie;
        pripajanieData.spustenieHry = &spustiHru;

        pthread_create(&vlaknoSpustanie, NULL, &spustenieHry, &spustanieData);
        pthread_create(&vlaknoPripajanie, NULL, &skusPripojit, &pripajanieData);

        pthread_join(vlaknoSpustanie, NULL);
        pthread_join(vlaknoPripajanie, NULL);

        pthread_mutex_destroy(&mutexPripajanie);
        printf("\n");

        //zacina hra

        //poslanie kazdemu hracovi ze hra zacina
        for (int i = 0; i < pocetHracov - 1; i++) {
            const char *msg = "Hra zacina!";
            n = write(hraci[i + 1].socketHraca, msg, strlen(msg) + 1);

            if (n < 0) {
                perror("Error writing to socket");
                return 5;
            }
        }

        inicializaciaIndexovPolicok();
        for (int i = 0; i < pocetHracov * 4; i++) {
            zaciatocnePolia[i] = 1;
        }

        //oznamenie kazdemu kolko hracov hra
        sleep(1);
        for (int i = 0; i < pocetHracov - 1; i++) {
            char typSpravy[5];
            sprintf(typSpravy, "%d", POCET_HRACOV_IDENTIFIKATOR);
            strcat(typSpravy, " ");

            char hodnota[5];
            sprintf(hodnota, "%d", pocetHracov);
            strcat(typSpravy, hodnota);

            const char *msg = typSpravy;
            n = write(hraci[i + 1].socketHraca, msg, strlen(msg) + 1);
            //n = write(sockfdHraci[i], msg, strlen(msg)+1);

            if (n < 0) {
                perror("Error writing to socket");
                return 5;
            }
        }
        pthread_t vlaknoHra;
        pthread_t vlaknoVykonavacSprav;
        pthread_t vlaknoCitanieSprav[pocetHracov - 1];

        pthread_mutex_t mutexClov;
        pthread_mutex_t mutexHra;
        pthread_mutex_init(&mutexClov, NULL);
        pthread_mutex_init(&mutexHra, NULL);

        pthread_cond_t condNacitajSpravu;
        pthread_cond_t condCitajPrijatuSpravu;
        pthread_cond_t condVykonajSpravu;
        pthread_cond_t condHraj;
        pthread_cond_init(&condNacitajSpravu, NULL);
        pthread_cond_init(&condCitajPrijatuSpravu, NULL);
        pthread_cond_init(&condVykonajSpravu, NULL);
        pthread_cond_init(&condHraj, NULL);

        int pocetNacitanych = 0;
        HRA hraData;
        hraData.condHraj = &condHraj;
        hraData.mutexHra = &mutexHra;
        hraData.condVykonajSpravu = &condVykonajSpravu;
        hraData.condCitajPrijatuSpravu = &condCitajPrijatuSpravu;
        pthread_create(&vlaknoHra, NULL, &hraj, &hraData);

        VYK_SPRAV vykonavacData;
        vykonavacData.condVykonajSpravu = &condVykonajSpravu;
        vykonavacData.condHraj = &condHraj;
        vykonavacData.condCitajPrijatuSpravu = &condCitajPrijatuSpravu;
        vykonavacData.condNacitajSpravu = &condNacitajSpravu;
        vykonavacData.mutexHra = &mutexHra;
        vykonavacData.mutexClovece = &mutexClov;
        vykonavacData.pocetNacitanych = &pocetNacitanych;
        pthread_create(&vlaknoVykonavacSprav, NULL, &vykonajSpravu, &vykonavacData);

        CIT_SPRAV citacSpravData[pocetHracov - 1];
        for (int i = 0; i < pocetHracov - 1; i++) {
            citacSpravData[i].pocetNacitanych = &pocetNacitanych;
            citacSpravData[i].condNacitajSpravu = &condNacitajSpravu;
            citacSpravData[i].condCitajPrijatuSpravu = &condCitajPrijatuSpravu;
            citacSpravData[i].mutexClovece = &mutexClov;
            citacSpravData[i].socket = hraci[i + 1].socketHraca;
            //printf("______socket hraca %d = %d\n", i+1, hraci[i + 1].socketHraca);
            pthread_create(&vlaknoCitanieSprav[i], NULL, &citajSpravy, &citacSpravData[i]);
        }

        pthread_join(vlaknoHra, NULL);
        pthread_join(vlaknoVykonavacSprav, NULL);
        for (int i = 0; i < pocetHracov; i++) {
            pthread_join(vlaknoCitanieSprav[i], NULL);
        }

        pthread_mutex_destroy(&mutexHra);
        pthread_mutex_destroy(&mutexClov);
        pthread_cond_destroy(&condCitajPrijatuSpravu);
        pthread_cond_destroy(&condHraj);
        pthread_cond_destroy(&condVykonajSpravu);
        pthread_cond_destroy(&condNacitajSpravu);
        printf("--------HRA KONCI------\n");

        for (int i = 0; i < MAX_POCET_KLIENTOV; i++) {
            close(hraci[i + 1].socketHraca);
        }
        //close(newsockfd);
        close(sockfd);
    } else if (atoi(argv[1]) == KLIENT) {
        //klientska cast
        struct sockaddr_in serv_addr;
        struct hostent *server;
        int oznacenieHraca = -1;

        if (argc < 4) {
            fprintf(stderr, "usage %s hostname port\n", argv[0]);
            return 1;
        }

        server = gethostbyname(argv[2]);
        if (server == NULL) {
            fprintf(stderr, "Error, no such host\n");
            return 2;
        }

        bzero((char *) &serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        bcopy(
                (char *) server->h_addr,
                (char *) &serv_addr.sin_addr.s_addr,
                server->h_length
        );
        serv_addr.sin_port = htons(atoi(argv[3]));

        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0) {
            perror("Error creating socket");
            return 3;
        }

        if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
            perror("Error connecting to socket");
            return 4;
        }
        //vytvorene spojenie

        printf("Zadaj meno hraca: ");
        bzero(buffer, 256);
        fgets(buffer, 255, stdin);

        n = write(sockfd, buffer, strlen(buffer));
        if (n < 0) {
            perror("Error writing to socket");
            return 5;
        }

        //zistenie oznacenia hraca ktory sa pripojil
        bzero(buffer, 256);
        n = read(sockfd, buffer, 255);
        oznacenieHraca = atoi(buffer) - 1;
        if (n < 0) {
            perror("Error reading from socket");
            return 6;
        }
        printf("Pripojil si sa ako hrac cislo %d\n", oznacenieHraca);

        //cakanie na to kym hra zacne
        bzero(buffer, 256);
        n = read(sockfd, buffer, 255);
        if (n < 0) {
            perror("Error reading from socket");
            return 6;
        }
        printf("%s\n", buffer);

        //zistenie poctu hracov
        precitajSpravu(sockfd);
        printf("Pocet hracov = %d\n", pocetHracov);

        //zacina hra
        inicializaciaIndexovPolicok();
        for (int i = 0; i < pocetHracov * 4; i++) {
            zaciatocnePolia[i] = 1;
        }

        pthread_t vlaknoHra;
        pthread_t vlaknoVykonavacSprav;
        pthread_t vlaknoCitanieSprav;

        pthread_mutex_t mutexClov;
        pthread_mutex_t mutexHra;
        pthread_mutex_init(&mutexClov, NULL);
        pthread_mutex_init(&mutexHra, NULL);

        pthread_cond_t condNacitajSpravu;
        pthread_cond_t condCitajPrijatuSpravu;
        pthread_cond_t condVykonajSpravu;
        pthread_cond_t condHraj;
        pthread_cond_init(&condNacitajSpravu, NULL);
        pthread_cond_init(&condCitajPrijatuSpravu, NULL);
        pthread_cond_init(&condVykonajSpravu, NULL);
        pthread_cond_init(&condHraj, NULL);

        int pocetNacitanych = 0;
        HRA_KLIENT hraData;
        hraData.condHraj = &condHraj;
        hraData.mutexHra = &mutexHra;
        hraData.condVykonajSpravu = &condVykonajSpravu;
        hraData.condCitajPrijatuSpravu = &condCitajPrijatuSpravu;
        hraData.oznacenieHraca = oznacenieHraca;
        pthread_create(&vlaknoHra, NULL, &klientHra, &hraData);

        VYK_SPRAV vykonavacData;
        vykonavacData.condVykonajSpravu = &condVykonajSpravu;
        vykonavacData.condHraj = &condHraj;
        vykonavacData.condCitajPrijatuSpravu = &condCitajPrijatuSpravu;
        vykonavacData.condNacitajSpravu = &condNacitajSpravu;
        vykonavacData.mutexHra = &mutexHra;
        vykonavacData.mutexClovece = &mutexClov;
        vykonavacData.pocetNacitanych = &pocetNacitanych;
        pthread_create(&vlaknoVykonavacSprav, NULL, &vykonajSpravu, &vykonavacData);

        CIT_SPRAV citacSpravData;
        citacSpravData.pocetNacitanych = &pocetNacitanych;
        citacSpravData.condNacitajSpravu = &condNacitajSpravu;
        citacSpravData.condCitajPrijatuSpravu = &condCitajPrijatuSpravu;
        citacSpravData.mutexClovece = &mutexClov;
        citacSpravData.socket = sockfd;
        pthread_create(&vlaknoCitanieSprav, NULL, &citajSpravy, &citacSpravData);

        pthread_join(vlaknoHra, NULL);
        pthread_join(vlaknoVykonavacSprav, NULL);
        pthread_join(vlaknoCitanieSprav, NULL);

        pthread_mutex_destroy(&mutexHra);
        pthread_mutex_destroy(&mutexClov);
        pthread_cond_destroy(&condCitajPrijatuSpravu);
        pthread_cond_destroy(&condHraj);
        pthread_cond_destroy(&condVykonajSpravu);
        pthread_cond_destroy(&condNacitajSpravu);
        printf("--------HRA KONCI------\n");

        close(sockfd);
    }
    return 0;
}

void inicializaciaIndexovPolicok() {
    for (int i = 0; i < POCET_FIGUROK; i++) {
        hraci[0].indexyPolicok[i] = -1;
        hraci[1].indexyPolicok[i] = -1;
        if (pocetHracov > 2) {
            hraci[2].indexyPolicok[i] = -1;
        } else {
            hraci[2].indexyPolicok[i] = HRAC_NEHRA;
        }
        if (pocetHracov > 3) {
            hraci[3].indexyPolicok[i] = -1;
        } else {
            hraci[3].indexyPolicok[i] = HRAC_NEHRA;
        }
    }
}

int hodKockou() {
    return rand() % (6) + 1;
}

const char *dajFarbuHraca(int indexHraca) {
    switch (indexHraca) {
        case 0:
            return "Zlty";
        case 1:
            return "Cerveny";
        case 2:
            return "Modry";
        case 3:
            return "Zeleny";
    }
}

int zistiPotencialnyTah(int figurka) {
    int potencialnyPohyb = -1;
    switch (hracNaRade) {
        case 0:
            potencialnyPohyb = hraci[0].indexyPolicok[figurka - 1] + hod;
            break;
        case 1:
            if ((hraci[1].indexyPolicok[figurka - 1] >= INDEX_CERVENY_HRAC) &&
                (hraci[1].indexyPolicok[figurka - 1] < POCET_POLI)) {
                potencialnyPohyb = (hraci[1].indexyPolicok[figurka - 1] + hod) % 40;
            } else {
                if ((hraci[1].indexyPolicok[figurka - 1] + hod) < INDEX_CERVENY_HRAC) {
                    potencialnyPohyb = (hraci[1].indexyPolicok[figurka - 1] + hod) % 40;
                } else {
                    potencialnyPohyb = (hraci[1].indexyPolicok[figurka - 1] + hod) % (INDEX_CERVENY_HRAC) + POCET_POLI;
                }
            }
            break;
        case 2:
            if ((hraci[2].indexyPolicok[figurka - 1] >= INDEX_MODRY_HRAC) &&
                (hraci[2].indexyPolicok[figurka - 1] < POCET_POLI)) {
                potencialnyPohyb = (hraci[2].indexyPolicok[figurka - 1] + hod) % 40;
            } else {
                if ((hraci[2].indexyPolicok[figurka - 1] + hod) < INDEX_MODRY_HRAC) {
                    potencialnyPohyb = (hraci[2].indexyPolicok[figurka - 1] + hod) % 40;
                } else {
                    potencialnyPohyb = (hraci[2].indexyPolicok[figurka - 1] + hod) % (INDEX_MODRY_HRAC) + POCET_POLI;
                }
            }
            break;
        case 3:
            if ((hraci[3].indexyPolicok[figurka - 1] >= INDEX_ZELENY_HRAC) &&
                (hraci[3].indexyPolicok[figurka - 1] < POCET_POLI)) {
                potencialnyPohyb = (hraci[3].indexyPolicok[figurka - 1] + hod) % 40;
            } else {
                if ((hraci[3].indexyPolicok[figurka - 1] + hod) < INDEX_ZELENY_HRAC) {
                    potencialnyPohyb = (hraci[3].indexyPolicok[figurka - 1] + hod) % 40;
                } else {
                    if (hraci[3].indexyPolicok[figurka - 1] >= POCET_POLI) {
                        potencialnyPohyb = hraci[3].indexyPolicok[figurka - 1] + hod;
                    } else {
                        potencialnyPohyb =
                                (hraci[3].indexyPolicok[figurka - 1] + hod) % (INDEX_ZELENY_HRAC) + POCET_POLI;
                    }
                }
            }
            break;
    }
    return potencialnyPohyb;
}

bool maAktualnyHracVsetkychPanacikovNaZaciatku() {
    int zaciatocnyIndex = -1;
    switch (hracNaRade) {
        case 0:
            zaciatocnyIndex = INDEX_ZLTY_HRAC;
            break;
        case 1:
            zaciatocnyIndex = INDEX_CERVENY_HRAC;
            break;
        case 2:
            zaciatocnyIndex = INDEX_MODRY_HRAC;
            break;
        case 3:
            zaciatocnyIndex = INDEX_ZELENY_HRAC;
            break;
    }
    for (int i = zaciatocnyIndex; i < (zaciatocnyIndex + POCET_FIGUROK); i++) {
        if (zaciatocnePolia[i] == PRAZDNE_POLE) {
            return false;
        }
    }
    return true;
}

int pohniFigurkou(int figurka) {
    switch (hracNaRade) {
        case 0: {
            //ak hodil 6 a dana fugurka je v domceku
            if ((hod == 6) && (zaciatocnePolia[INDEX_ZACIATOK_ZLTY_HRAC + figurka - 1] == OBSADENE_POLE)) {
                //ak je uz startovne pole obsadene panacikom tohto hraca
                if (hraciePolia[INDEX_ZLTY_HRAC][HRAC_INFO] == hracNaRade) {
                    printf("Pole je obsadene jeho panacikom.");
                    return NEMOZNY_POHYB;
                } else {
                    //ak je pole volne
                    if ((hraciePolia[INDEX_ZLTY_HRAC][HRAC_INFO] == PRAZDNE_POLE) &&
                        (hraciePolia[INDEX_ZLTY_HRAC][FIGURKA_INFO] = PRAZDNE_POLE)) {
                        zaciatocnePolia[INDEX_ZACIATOK_ZLTY_HRAC + figurka - 1] = PRAZDNE_POLE;
                        hraci[0].indexyPolicok[figurka - 1] = INDEX_ZLTY_HRAC;
                        /*hraciePolia[INDEX_ZLTY_HRAC][HRAC_INFO] = hracNaRade;
                        hraciePolia[INDEX_ZLTY_HRAC][FIGURKA_INFO] = figurka - 1;
                        return INDEX_ZLTY_HRAC;*/
                    } else {
                        //pole je obsadene inym hracom(ide ho vyhodit)
                        int hracVyhadzovanehoPanacika = hraciePolia[INDEX_ZLTY_HRAC][HRAC_INFO];
                        int vyhadzovanyPancaik = hraciePolia[INDEX_ZLTY_HRAC][FIGURKA_INFO];
                        printf("vyhadzuje hrac 1(index 0)");
                        printf("\n Vyhodeny panacik %d hraca %d\n", vyhadzovanyPancaik, hracVyhadzovanehoPanacika);
                        vyhodPanacika(hracVyhadzovanehoPanacika, vyhadzovanyPancaik);
                        zaciatocnePolia[0 + figurka - 1] = PRAZDNE_POLE;
                        hraci[0].indexyPolicok[figurka - 1] = INDEX_ZLTY_HRAC;
                        /*hraciePolia[INDEX_ZLTY_HRAC][HRAC_INFO] = hracNaRade;
                        hraciePolia[INDEX_ZLTY_HRAC][FIGURKA_INFO] = figurka - 1;
                        return INDEX_ZLTY_HRAC;*/
                    }
                    hraciePolia[INDEX_ZLTY_HRAC][HRAC_INFO] = hracNaRade;
                    hraciePolia[INDEX_ZLTY_HRAC][FIGURKA_INFO] = figurka - 1;
                    return INDEX_ZLTY_HRAC;
                }

            } else {
                //nehodil 6 tak nemoze dat panaka do hry
                if (zaciatocnePolia[INDEX_ZACIATOK_ZLTY_HRAC + figurka - 1] == OBSADENE_POLE) {
                    return NEMOZNY_POHYB;
                }
                int aktualnePolePanacika = hraci[0].indexyPolicok[figurka - 1];
                int potencialnePole = zistiPotencialnyTah(figurka);
                //ked nenaslo potencialne pole
                if (potencialnePole == NEMOZNY_POHYB) {
                    return NEMOZNY_POHYB;
                } else {
                    //ak je zo zakladnych poli
                    if (potencialnePole < POCET_POLI) {
                        //ak sa na poli kam sa presuva nikto nie je
                        if ((hraciePolia[potencialnePole][HRAC_INFO] == PRAZDNE_POLE) &&
                            (hraciePolia[potencialnePole][FIGURKA_INFO] = PRAZDNE_POLE)) {
                            hraciePolia[aktualnePolePanacika][HRAC_INFO] = PRAZDNE_POLE;
                            hraciePolia[aktualnePolePanacika][FIGURKA_INFO] = PRAZDNE_POLE;
                            hraciePolia[potencialnePole][HRAC_INFO] = hracNaRade;
                            hraciePolia[potencialnePole][FIGURKA_INFO] = figurka - 1;
                            hraci[0].indexyPolicok[figurka - 1] = potencialnePole;
                            return potencialnePole;
                        } else {
                            //ak je na poli kde sa chce presunut dalsi panacik daneho hraca
                            if (hraciePolia[potencialnePole][HRAC_INFO] == hracNaRade) {
                                return NEMOZNY_POHYB;
                            } else {
                                //na poli je panacik ineho hraca
                                hraciePolia[aktualnePolePanacika][HRAC_INFO] = PRAZDNE_POLE;
                                hraciePolia[aktualnePolePanacika][FIGURKA_INFO] = PRAZDNE_POLE;
                                int hracVyhadzovanehoPanacika = hraciePolia[potencialnePole][HRAC_INFO];
                                int vyhadzovanyPancaik = hraciePolia[potencialnePole][FIGURKA_INFO];
                                printf("vyhadzuje hrac 1(index 0)");
                                printf("\n Vyhodeny panacik %d hraca %d\n", vyhadzovanyPancaik,
                                       hracVyhadzovanehoPanacika);
                                vyhodPanacika(hracVyhadzovanehoPanacika, vyhadzovanyPancaik);
                                hraciePolia[potencialnePole][HRAC_INFO] = hracNaRade;
                                hraciePolia[potencialnePole][FIGURKA_INFO] = figurka - 1;
                                hraci[0].indexyPolicok[figurka - 1] = potencialnePole;
                                return potencialnePole;
                            }
                        }
                    } else {
                        int poleVDomceku = potencialnePole % POCET_POLI;
                        if ((poleVDomceku < POCET_FIGUROK) &&
                            (finalnePolia[INDEX_DOMCEK_ZLTY_HRAC + poleVDomceku] == PRAZDNE_POLE)) {
                            finalnePolia[INDEX_DOMCEK_ZLTY_HRAC + poleVDomceku] = OBSADENE_POLE;
                            hraci[0].indexyPolicok[figurka - 1] = potencialnePole;
                            //panacik sa dostava do domceku
                            if (aktualnePolePanacika < POCET_POLI) {
                                //ak sa aktualne nachadza mimo domceka a prechadza donho teraz
                                hraciePolia[aktualnePolePanacika][HRAC_INFO] = PRAZDNE_POLE;
                                hraciePolia[aktualnePolePanacika][FIGURKA_INFO] = PRAZDNE_POLE;
                            } else {
                                //ak sa aktualne nachadza uz v domceku a presuva sa v nom
                                finalnePolia[INDEX_DOMCEK_ZLTY_HRAC + aktualnePolePanacika % POCET_POLI] = PRAZDNE_POLE;
                            }
                            return potencialnePole;
                        } else {
                            return NEMOZNY_POHYB;
                        }
                    }
                }
            }
        }
        case 1: {
            if ((hod == 6) && (zaciatocnePolia[INDEX_ZACIATOK_CERVENY_HRAC + figurka - 1] == OBSADENE_POLE)) {
                //ak je uz startovne pole obsadene panacikom tohto hraca
                if (hraciePolia[INDEX_CERVENY_HRAC][HRAC_INFO] == hracNaRade) {
                    printf("Pole je obsadene jeho panacikom.");
                    return NEMOZNY_POHYB;
                } else {
                    //ak je pole volne
                    if ((hraciePolia[INDEX_CERVENY_HRAC][HRAC_INFO] == PRAZDNE_POLE) &&
                        (hraciePolia[INDEX_CERVENY_HRAC][FIGURKA_INFO] = PRAZDNE_POLE)) {
                        hraciePolia[INDEX_CERVENY_HRAC][HRAC_INFO] = hracNaRade;
                        hraciePolia[INDEX_CERVENY_HRAC][FIGURKA_INFO] = figurka - 1;
                        zaciatocnePolia[INDEX_ZACIATOK_CERVENY_HRAC + figurka - 1] = PRAZDNE_POLE;
                        hraci[1].indexyPolicok[figurka - 1] = INDEX_CERVENY_HRAC;
                        return INDEX_CERVENY_HRAC;
                    } else {
                        //pole je obsadene inym hracom(ide ho vyhodit)
                        int hracVyhadzovanehoPanacika = hraciePolia[INDEX_CERVENY_HRAC][HRAC_INFO];
                        int vyhadzovanyPancaik = hraciePolia[INDEX_CERVENY_HRAC][FIGURKA_INFO];
                        printf("\n Vyhodeny panacik %d hraca %d\n", vyhadzovanyPancaik, hracVyhadzovanehoPanacika);
                        vyhodPanacika(hracVyhadzovanehoPanacika, vyhadzovanyPancaik);
                        zaciatocnePolia[INDEX_ZACIATOK_CERVENY_HRAC + figurka - 1] = PRAZDNE_POLE;
                        hraci[1].indexyPolicok[figurka - 1] = INDEX_CERVENY_HRAC;
                        hraciePolia[INDEX_CERVENY_HRAC][HRAC_INFO] = hracNaRade;
                        hraciePolia[INDEX_CERVENY_HRAC][FIGURKA_INFO] = figurka - 1;
                        return INDEX_CERVENY_HRAC;
                    }
                }
            } else {
                //nehodil 6 tak nemoze dat panaka do hry
                if (zaciatocnePolia[INDEX_ZACIATOK_CERVENY_HRAC + figurka - 1] == OBSADENE_POLE) {
                    return NEMOZNY_POHYB;
                }
                int aktualnePolePanacika = hraci[1].indexyPolicok[figurka - 1];
                int potencialnePole = zistiPotencialnyTah(figurka);
                //ked nenaslo potencialne pole
                if (potencialnePole == NEMOZNY_POHYB) {
                    return NEMOZNY_POHYB;
                } else {
                    //ak je zo zakladnych poli
                    if (potencialnePole < POCET_POLI) {
                        //ak sa na poli kam sa presuva nikto nie je
                        if ((hraciePolia[potencialnePole][HRAC_INFO] == PRAZDNE_POLE) &&
                            (hraciePolia[potencialnePole][FIGURKA_INFO] = PRAZDNE_POLE)) {
                            hraciePolia[aktualnePolePanacika][HRAC_INFO] = PRAZDNE_POLE;
                            hraciePolia[aktualnePolePanacika][FIGURKA_INFO] = PRAZDNE_POLE;
                            hraciePolia[potencialnePole][HRAC_INFO] = hracNaRade;
                            hraciePolia[potencialnePole][FIGURKA_INFO] = figurka - 1;
                            hraci[1].indexyPolicok[figurka - 1] = potencialnePole;
                            return potencialnePole;
                        } else {
                            //ak je na poli kde sa chce presunut dalsi panacik daneho hraca
                            if (hraciePolia[potencialnePole][HRAC_INFO] == hracNaRade) {
                                return NEMOZNY_POHYB;
                            } else {
                                //na poli je panacik ineho hraca
                                hraciePolia[aktualnePolePanacika][HRAC_INFO] = PRAZDNE_POLE;
                                hraciePolia[aktualnePolePanacika][FIGURKA_INFO] = PRAZDNE_POLE;
                                int hracVyhadzovanehoPanacika = hraciePolia[potencialnePole][HRAC_INFO];
                                int vyhadzovanyPancaik = hraciePolia[potencialnePole][FIGURKA_INFO];
                                printf("vyhadzuje hrac 2(index 1)");
                                printf("\n Vyhodeny panacik %d hraca %d\n", vyhadzovanyPancaik,
                                       hracVyhadzovanehoPanacika);
                                vyhodPanacika(hracVyhadzovanehoPanacika, vyhadzovanyPancaik);
                                hraciePolia[potencialnePole][HRAC_INFO] = hracNaRade;
                                hraciePolia[potencialnePole][FIGURKA_INFO] = figurka - 1;
                                hraci[1].indexyPolicok[figurka - 1] = potencialnePole;
                                return potencialnePole;
                            }
                        }
                    } else {
                        int poleVDomceku = potencialnePole % POCET_POLI;
                        if ((poleVDomceku < POCET_FIGUROK) &&
                            (finalnePolia[INDEX_DOMCEK_CERVENY_HRAC + poleVDomceku] == -1)) {
                            //panacik je v domceku
                            finalnePolia[INDEX_DOMCEK_CERVENY_HRAC + poleVDomceku] = 1;
                            hraci[1].indexyPolicok[figurka - 1] = potencialnePole;
                            //panacik sa dostava do domceku
                            if (aktualnePolePanacika < POCET_POLI) {
                                //ak sa aktualne nachadza mimo domceka a prechadza donho teraz
                                hraciePolia[aktualnePolePanacika][HRAC_INFO] = PRAZDNE_POLE;
                                hraciePolia[aktualnePolePanacika][FIGURKA_INFO] = PRAZDNE_POLE;
                            } else {
                                //ak sa aktualne nachadza uz v domceku a presuva sa v nom
                                finalnePolia[INDEX_DOMCEK_CERVENY_HRAC +
                                             aktualnePolePanacika % POCET_POLI] = PRAZDNE_POLE;
                            }
                            return potencialnePole;
                        } else {
                            return NEMOZNY_POHYB;
                        }
                    }
                }
            }
        }
        case 2: {
            if ((hod == 6) && (zaciatocnePolia[INDEX_ZACIATOK_MODRY_HRAC + figurka - 1] == OBSADENE_POLE)) {
                //ak je uz startovne pole obsadene panacikom tohto hraca
                if (hraciePolia[INDEX_MODRY_HRAC][HRAC_INFO] == hracNaRade) {
                    printf("Pole je obsadene jeho panacikom.");
                    return NEMOZNY_POHYB;
                } else {
                    //ak je pole volne
                    if ((hraciePolia[INDEX_MODRY_HRAC][HRAC_INFO] == PRAZDNE_POLE) &&
                        (hraciePolia[INDEX_MODRY_HRAC][FIGURKA_INFO] = PRAZDNE_POLE)) {
                        hraciePolia[INDEX_MODRY_HRAC][HRAC_INFO] = hracNaRade;
                        hraciePolia[INDEX_MODRY_HRAC][FIGURKA_INFO] = figurka - 1;
                        zaciatocnePolia[INDEX_ZACIATOK_MODRY_HRAC + figurka - 1] = PRAZDNE_POLE;
                        hraci[2].indexyPolicok[figurka - 1] = INDEX_MODRY_HRAC;
                        return INDEX_MODRY_HRAC;
                    } else {
                        //pole je obsadene inym hracom(ide ho vyhodit)
                        int hracVyhadzovanehoPanacika = hraciePolia[INDEX_MODRY_HRAC][HRAC_INFO];
                        int vyhadzovanyPancaik = hraciePolia[INDEX_MODRY_HRAC][FIGURKA_INFO];
                        printf("\n Vyhodeny panacik %d hraca %d\n", vyhadzovanyPancaik, hracVyhadzovanehoPanacika);
                        vyhodPanacika(hracVyhadzovanehoPanacika, vyhadzovanyPancaik);
                        zaciatocnePolia[INDEX_ZACIATOK_MODRY_HRAC + figurka - 1] = PRAZDNE_POLE;
                        hraci[2].indexyPolicok[figurka - 1] = INDEX_MODRY_HRAC;
                        hraciePolia[INDEX_MODRY_HRAC][HRAC_INFO] = hracNaRade;
                        hraciePolia[INDEX_MODRY_HRAC][FIGURKA_INFO] = figurka - 1;
                        return INDEX_MODRY_HRAC;
                    }
                }
            } else {
                //nehodil 6 tak nemoze dat panaka do hry
                if (zaciatocnePolia[INDEX_ZACIATOK_MODRY_HRAC + figurka - 1] == OBSADENE_POLE) {
                    return NEMOZNY_POHYB;
                }
                int aktualnePolePanacika = hraci[2].indexyPolicok[figurka - 1];
                int potencialnePole = zistiPotencialnyTah(figurka);
                //ked nenaslo potencialne pole
                if (potencialnePole == NEMOZNY_POHYB) {
                    return NEMOZNY_POHYB;
                } else {
                    //ak je zo zakladnych poli
                    if (potencialnePole < POCET_POLI) {
                        //ak sa na poli kam sa presuva nikto nie je
                        if ((hraciePolia[potencialnePole][HRAC_INFO] == PRAZDNE_POLE) &&
                            (hraciePolia[potencialnePole][FIGURKA_INFO] = PRAZDNE_POLE)) {
                            hraciePolia[aktualnePolePanacika][HRAC_INFO] = PRAZDNE_POLE;
                            hraciePolia[aktualnePolePanacika][FIGURKA_INFO] = PRAZDNE_POLE;
                            hraciePolia[potencialnePole][HRAC_INFO] = hracNaRade;
                            hraciePolia[potencialnePole][FIGURKA_INFO] = figurka - 1;
                            hraci[2].indexyPolicok[figurka - 1] = potencialnePole;
                            return potencialnePole;
                        } else {
                            //ak je na poli kde sa chce presunut dalsi panacik daneho hraca
                            if (hraciePolia[potencialnePole][HRAC_INFO] == hracNaRade) {
                                return NEMOZNY_POHYB;
                            } else {
                                //na poli je panacik ineho hraca
                                hraciePolia[aktualnePolePanacika][HRAC_INFO] = PRAZDNE_POLE;
                                hraciePolia[aktualnePolePanacika][FIGURKA_INFO] = PRAZDNE_POLE;
                                int hracVyhadzovanehoPanacika = hraciePolia[potencialnePole][HRAC_INFO];
                                int vyhadzovanyPancaik = hraciePolia[potencialnePole][FIGURKA_INFO];
                                printf("\n Vyhodeny panacik %d hraca %d\n", vyhadzovanyPancaik,
                                       hracVyhadzovanehoPanacika);
                                vyhodPanacika(hracVyhadzovanehoPanacika, vyhadzovanyPancaik);
                                hraciePolia[potencialnePole][HRAC_INFO] = hracNaRade;
                                hraciePolia[potencialnePole][FIGURKA_INFO] = figurka - 1;
                                hraci[2].indexyPolicok[figurka - 1] = potencialnePole;
                                return potencialnePole;
                            }
                        }
                    } else {
                        int poleVDomceku = potencialnePole % POCET_POLI;
                        if ((poleVDomceku < POCET_FIGUROK) &&
                            (finalnePolia[INDEX_DOMCEK_MODRY_HRAC + poleVDomceku] == -1)) {
                            //panacik je v domceku
                            finalnePolia[INDEX_DOMCEK_MODRY_HRAC + poleVDomceku] = 1;
                            hraci[2].indexyPolicok[figurka - 1] = potencialnePole;
                            //panacik sa dostava do domceku
                            if (aktualnePolePanacika < POCET_POLI) {
                                //ak sa aktualne nachadza mimo domceka a prechadza donho teraz
                                hraciePolia[aktualnePolePanacika][HRAC_INFO] = PRAZDNE_POLE;
                                hraciePolia[aktualnePolePanacika][FIGURKA_INFO] = PRAZDNE_POLE;
                            } else {
                                //ak sa aktualne nachadza uz v domceku a presuva sa v nom
                                finalnePolia[INDEX_DOMCEK_MODRY_HRAC +
                                             aktualnePolePanacika % POCET_POLI] = PRAZDNE_POLE;
                            }
                            return potencialnePole;
                        } else {
                            return NEMOZNY_POHYB;
                        }
                    }
                }
            }
        }
        case 3: {
            if ((hod == 6) && (zaciatocnePolia[INDEX_ZACIATOK_ZELENY_HRAC + figurka - 1] == OBSADENE_POLE)) {
                //ak je uz startovne pole obsadene panacikom tohto hraca
                if (hraciePolia[INDEX_ZELENY_HRAC][HRAC_INFO] == hracNaRade) {
                    printf("Pole je obsadene jeho panacikom.");
                    return NEMOZNY_POHYB;
                } else {
                    //ak je pole volne
                    if ((hraciePolia[INDEX_ZELENY_HRAC][HRAC_INFO] == PRAZDNE_POLE) &&
                        (hraciePolia[INDEX_ZELENY_HRAC][FIGURKA_INFO] = PRAZDNE_POLE)) {
                        hraciePolia[INDEX_ZELENY_HRAC][HRAC_INFO] = hracNaRade;
                        hraciePolia[INDEX_ZELENY_HRAC][FIGURKA_INFO] = figurka - 1;
                        zaciatocnePolia[INDEX_ZACIATOK_ZELENY_HRAC + figurka - 1] = PRAZDNE_POLE;
                        hraci[3].indexyPolicok[figurka - 1] = INDEX_ZELENY_HRAC;
                        return INDEX_ZELENY_HRAC;
                    } else {
                        //pole je obsadene inym hracom(ide ho vyhodit)
                        int hracVyhadzovanehoPanacika = hraciePolia[INDEX_ZELENY_HRAC][HRAC_INFO];
                        int vyhadzovanyPancaik = hraciePolia[INDEX_ZELENY_HRAC][FIGURKA_INFO];
                        printf("\n Vyhodeny panacik %d hraca %d\n", vyhadzovanyPancaik, hracVyhadzovanehoPanacika);
                        vyhodPanacika(hracVyhadzovanehoPanacika, vyhadzovanyPancaik);
                        zaciatocnePolia[INDEX_ZACIATOK_ZELENY_HRAC + figurka - 1] = PRAZDNE_POLE;
                        hraci[3].indexyPolicok[figurka - 1] = INDEX_ZELENY_HRAC;
                        hraciePolia[INDEX_ZELENY_HRAC][HRAC_INFO] = hracNaRade;
                        hraciePolia[INDEX_ZELENY_HRAC][FIGURKA_INFO] = figurka - 1;
                        return INDEX_ZELENY_HRAC;
                    }
                }
            } else {
                //nehodil 6 tak nemoze dat panaka do hry
                if (zaciatocnePolia[INDEX_ZACIATOK_ZELENY_HRAC + figurka - 1] == OBSADENE_POLE) {
                    return NEMOZNY_POHYB;
                }
                int aktualnePolePanacika = hraci[3].indexyPolicok[figurka - 1];
                int potencialnePole = zistiPotencialnyTah(figurka);
                //ked nenaslo potencialne pole
                if (potencialnePole == NEMOZNY_POHYB) {
                    return NEMOZNY_POHYB;
                } else {
                    //ak je zo zakladnych poli
                    if (potencialnePole < POCET_POLI) {
                        //ak sa na poli kam sa presuva nikto nie je
                        if ((hraciePolia[potencialnePole][HRAC_INFO] == PRAZDNE_POLE) &&
                            (hraciePolia[potencialnePole][FIGURKA_INFO] = PRAZDNE_POLE)) {
                            hraciePolia[aktualnePolePanacika][HRAC_INFO] = PRAZDNE_POLE;
                            hraciePolia[aktualnePolePanacika][FIGURKA_INFO] = PRAZDNE_POLE;
                            hraciePolia[potencialnePole][HRAC_INFO] = hracNaRade;
                            hraciePolia[potencialnePole][FIGURKA_INFO] = figurka - 1;
                            hraci[3].indexyPolicok[figurka - 1] = potencialnePole;
                            return potencialnePole;
                        } else {
                            //ak je na poli kde sa chce presunut dalsi panacik daneho hraca
                            if (hraciePolia[potencialnePole][HRAC_INFO] == hracNaRade) {
                                return NEMOZNY_POHYB;
                            } else {
                                //na poli je panacik ineho hraca
                                hraciePolia[aktualnePolePanacika][HRAC_INFO] = PRAZDNE_POLE;
                                hraciePolia[aktualnePolePanacika][FIGURKA_INFO] = PRAZDNE_POLE;
                                int hracVyhadzovanehoPanacika = hraciePolia[potencialnePole][HRAC_INFO];
                                int vyhadzovanyPancaik = hraciePolia[potencialnePole][FIGURKA_INFO];
                                printf("\n Vyhodeny panacik %d hraca %d\n", vyhadzovanyPancaik,
                                       hracVyhadzovanehoPanacika);
                                vyhodPanacika(hracVyhadzovanehoPanacika, vyhadzovanyPancaik);
                                hraciePolia[potencialnePole][HRAC_INFO] = hracNaRade;
                                hraciePolia[potencialnePole][FIGURKA_INFO] = figurka - 1;
                                hraci[3].indexyPolicok[figurka - 1] = potencialnePole;
                                return potencialnePole;
                            }
                        }
                    } else {
                        int poleVDomceku = potencialnePole % POCET_POLI;
                        if ((poleVDomceku < POCET_FIGUROK) &&
                            (finalnePolia[INDEX_DOMCEK_ZELENY_HRAC + poleVDomceku] == -1)) {
                            //panacik je v domceku
                            finalnePolia[INDEX_DOMCEK_ZELENY_HRAC + poleVDomceku] = 1;
                            hraci[3].indexyPolicok[figurka - 1] = potencialnePole;
                            //panacik sa dostava do domceku
                            if (aktualnePolePanacika < POCET_POLI) {
                                //ak sa aktualne nachadza mimo domceka a prechadza donho teraz
                                hraciePolia[aktualnePolePanacika][HRAC_INFO] = PRAZDNE_POLE;
                                hraciePolia[aktualnePolePanacika][FIGURKA_INFO] = PRAZDNE_POLE;
                            } else {
                                //ak sa aktualne nachadza uz v domceku a presuva sa v nom
                                finalnePolia[INDEX_DOMCEK_ZELENY_HRAC +
                                             aktualnePolePanacika % POCET_POLI] = PRAZDNE_POLE;
                            }
                            return potencialnePole;
                        } else {
                            return NEMOZNY_POHYB;
                        }
                    }
                }
            }
        }
    }
    return -1;
}

void vyhodPanacika(int hracVyhadzovanehoPanacika, int vyhadzovanyPanacik) {
    switch (hracVyhadzovanehoPanacika) {
        case 0:
            hraci[0].indexyPolicok[vyhadzovanyPanacik] = NA_ZACIATOCNOM_POLI;
            zaciatocnePolia[INDEX_ZACIATOK_ZLTY_HRAC + vyhadzovanyPanacik] = OBSADENE_POLE;
            break;
        case 1:
            hraci[1].indexyPolicok[vyhadzovanyPanacik] = NA_ZACIATOCNOM_POLI;
            zaciatocnePolia[INDEX_ZACIATOK_CERVENY_HRAC + vyhadzovanyPanacik] = OBSADENE_POLE;
            break;
        case 2:
            hraci[2].indexyPolicok[vyhadzovanyPanacik] = NA_ZACIATOCNOM_POLI;
            zaciatocnePolia[INDEX_ZACIATOK_MODRY_HRAC + vyhadzovanyPanacik] = OBSADENE_POLE;
            break;
        case 3:
            hraci[3].indexyPolicok[vyhadzovanyPanacik] = NA_ZACIATOCNOM_POLI;
            zaciatocnePolia[INDEX_ZACIATOK_ZELENY_HRAC + vyhadzovanyPanacik] = OBSADENE_POLE;
            break;
    }
}

void vykresleniePolicok() {
    //zakladny vypis policok
    printf("\n-----------------------------------------------------------------\n");
    printf("Zaciatocne polia hracov:");
    for (int i = 0; i < MAX_POCET_HRACOV * POCET_FIGUROK; ++i) {
        if (i == INDEX_ZACIATOK_ZLTY_HRAC) {
            printf("\nFigurky zlteho hraca\n");
        } else if (i == INDEX_ZACIATOK_CERVENY_HRAC) {
            printf("\nFigurky cerveneho hraca\n");
        } else if (i == INDEX_ZACIATOK_MODRY_HRAC) {
            printf("\nFigurky modreho hraca\n");
        } else if (i == INDEX_ZACIATOK_ZELENY_HRAC) {
            printf("\nFigurky zeleneho hraca\n");
        }
        //printf("|%d|", zaciatocnePolia[i]);
        //vypisanie panacikov ktorými este hrac moze pohnut

        if (zaciatocnePolia[i] == 1) {
            if (i < 4) {
                printf("|Z|");
            }
            if (i >= 4 && i < 8) {
                printf("|C|");
            }
            if (i >= 8 && i < 12) {
                printf("|M|");
            }
            if (i >= 12 && i < 16) {
                printf("|Ze|");
            }
        } else {
            printf("| |");
        }
    }
    printf("\n-----------------------------------------------------------------\n");

    /*
    printf("Hracie polia:\n");
    for (int i = 0; i < POCET_POLI; i++) {
        if (hraciePolia[i][0] == -1) {
            printf("|%d=%d,%d|\n", i, hraciePolia[i][0], hraciePolia[i][1]);
        } else {
            printf("|%d=%d,%d|<--figurka\n", i, hraciePolia[i][0], hraciePolia[i][1]);
        }
    }
    */

    printf("---------------------------\n");
    printf("--HRACIE POLE--\n");

    //vypisanie hracieho pola na konzolu
    for (int i = 0; i < POCET_POLI; i++) {
        if (hraciePolia[i][0] == -1) {
            if (i < POCET_POLI-1 && i == 0) {
                //zaciatocne policko 1. hraca
                printf("Start Zlty || || - ");
            } else if (i < POCET_POLI-1 && i == 10) {
                //zaciatocne policko 2. hraca
                printf("Start Cerveny || || - ");
            } else if (i < POCET_POLI-1 && i == 20) {
                //zaciatocne policko 3. hraca
                printf("Start Modry || || - ");
            } else if (i < POCET_POLI-1 && i == 30) {
                //zaciatocne policko 4. hraca
                printf("Start Zeleny || || - ");
            } else if (i < POCET_POLI-1) {
                //policko kde nie je figurka hraca je reprezentovana | |
                printf("| | - ");
            } else {
                //posledne policko na hracej ploche
                printf("| |");
            }
        } else { //ak sa na policku nachadza hrac tak zobrazi jeho prislusny znak
            if (hraciePolia[i][0] == 0 && i == 0) {
                printf("Start Zlty ||Z|| - ");
                //vypisanie startovneho policka ak je tam figurka daneho hraca ktoremu patri
            } else if (hraciePolia[i][0] == 1 && i == 10) {
                printf("Start Cerveny ||C|| - ");
            } else if (hraciePolia[i][0] == 2 && i == 20) {
                printf("Start Modry ||M|| - ");
            } else if (hraciePolia[i][0] == 3 && i == 30) {
                printf("Start Zeleny ||Ze|| - ");

                //vypis ak vstupi niekto iny na startovacie policko zlteho
            } else if (hraciePolia[i][0] == 1 && i == 0) {
                printf("Start Zlty ||C|| - ");
            } else if (hraciePolia[i][0] == 2 && i == 0) {
                printf("Start Zlty ||M|| - ");
            } else if (hraciePolia[i][0] == 3 && i == 0) {
                printf("Start Zlty ||Ze|| - ");

                //vypis ak vstupi niekto iny na startovacie policko cerveneho
            } else if (hraciePolia[i][0] == 0 && i == 10) {
                printf("Start Cerveny ||Z|| - ");
            } else if (hraciePolia[i][0] == 2 && i == 10) {
                printf("Start Cerveny ||M|| - ");
            } else if (hraciePolia[i][0] == 3 && i == 10) {
                printf("Start Cerveny ||Ze|| - ");

                //vypis ak vstupi niekto iny na startovacie policko modreho
            } else if (hraciePolia[i][0] == 0 && i == 20) {
                printf("Start Modry ||Z|| - ");
            } else if (hraciePolia[i][0] == 1 && i == 20) {
                printf("Start Modry ||C|| - ");
            } else if (hraciePolia[i][0] == 3 && i == 20) {
                printf("Start Modry ||Ze|| - ");

                //vypis ak vstupi niekto iny na startovacie policko zeleneho
            } else if (hraciePolia[i][0] == 0 && i == 30) {
                printf("Start Zeleny ||Z|| - ");
            } else if (hraciePolia[i][0] == 1 && i == 30) {
                printf("Start Zeleny ||C|| - ");
            } else if (hraciePolia[i][0] == 2 && i == 30) {
                printf("Start Zeleny ||M|| - ");

                //vypis mimo startovacich policok
            } else if (hraciePolia[i][0] == 0) {
                printf("|Z| - ");
            } else if (hraciePolia[i][0] == 1) {
                printf("|C| - ");
            } else if (hraciePolia[i][0] == 2) {
                printf("|M| - ");
            } else if (hraciePolia[i][0] == 3) {
                printf("|Ze| - ");
            } else {
                printf("!!!Chyba!!!");
            }
        }
    }
    printf("\n");

    printf("---------------------------\n");
    printf("\n");
    printf("-----------------------------------------------------------------\n");
    printf("Domceky hracov:");
    for (int i = 0; i < MAX_POCET_HRACOV * POCET_FIGUROK; ++i) {
        if (i == INDEX_DOMCEK_ZLTY_HRAC) {
            printf("\nFigurky zlteho hraca\n");
        } else if (i == INDEX_DOMCEK_CERVENY_HRAC) {
            printf("\nFigurky cerveneho hraca\n");
        } else if (i == INDEX_DOMCEK_MODRY_HRAC) {
            printf("\nFigurky modreho hraca\n");
        } else if (i == INDEX_DOMCEK_ZELENY_HRAC) {
            printf("\nFigurky zeleneho hraca\n");
        }
        //printf("|%d|", finalnePolia[i]);
        //vypisanie domcekov
        if (finalnePolia[i] == 1) {
            if (i < 4) {
                printf("- |Z| ");
            }
            if (i >= 4 && i < 8) {
                printf("- |C| ");
            }
            if (i >= 8 && i < 12) {
                printf("- |M| ");
            }
            if (i >= 12 && i < 16) {
                printf("- |Ze| ");
            }
        } else {
            printf("- | | ");
        }

    }
    printf("\n-----------------------------------------------------------------\n");
}

int zistiVitaza() {
    int pocetFigurokVDomceku[MAX_POCET_HRACOV] = {0, 0, 0, 0};
    for (int i = 0; i < pocetHracov * POCET_FIGUROK; i++) {
        if ((i < (INDEX_DOMCEK_ZLTY_HRAC + POCET_FIGUROK)) && (finalnePolia[i] == 1)) {
            pocetFigurokVDomceku[0]++;
        }
        if ((i < (INDEX_DOMCEK_CERVENY_HRAC + POCET_FIGUROK)) && (i >= (INDEX_DOMCEK_ZLTY_HRAC + POCET_FIGUROK)) &&
            (finalnePolia[i] == 1)) {
            pocetFigurokVDomceku[1]++;
        }
        if ((i < (INDEX_DOMCEK_MODRY_HRAC + POCET_FIGUROK)) && (i >= (INDEX_DOMCEK_CERVENY_HRAC + POCET_FIGUROK)) &&
            (finalnePolia[i] == 1)) {
            pocetFigurokVDomceku[2]++;
        }
        if ((i < (INDEX_DOMCEK_ZELENY_HRAC + POCET_FIGUROK)) && (i >= (INDEX_DOMCEK_MODRY_HRAC + POCET_FIGUROK)) &&
            (finalnePolia[i] == 1)) {
            pocetFigurokVDomceku[3]++;
        }
    }
    for (int i = 0; i < MAX_POCET_HRACOV; i++) {
        if (pocetFigurokVDomceku[i] == POCET_FIGUROK) {
            //vrati index vitazneho hraca;
            return i;
        }
    }
    //ak nikto este nevyhral
    return -1;
}

void precitajSpravu(int socket) {
    bzero(buffer, 256);
    n = read(socket, buffer, 255);
    const char oddelovac[2] = " ";
    char *token;
    token = strtok(buffer, oddelovac);
    //najprv je oznacenie typu prijatej spravy
    const char *typSpravy = token;
    //printf("Typ spravy = %s\n", token);
    token = strtok(NULL, oddelovac);
    //potom hodnota pre spravu
    const char *hodnotaSpravy = token;

    switch (atoi(typSpravy)) {
        case POCET_HRACOV_IDENTIFIKATOR:
            pocetHracov = atoi(hodnotaSpravy);
            break;
        case HRAC_NA_RADE_IDENTIFIKATOR:
            hracNaRade = atoi(hodnotaSpravy);
            //printf("Precitany hrac na rade = %d\n", hracNaRade);
            break;
        case HOD_HRACA_IDENTIFIKATOR:
            hod = atoi(hodnotaSpravy);
            printf("Zistena hodnota hodu hraca = %d\n", hod);
            break;
        case POHYB_FIGURKOU_IDENTIFIKATOR:
            aktualnaFigurka = atoi(hodnotaSpravy);
            printf("Zistene ktorou figurkou chce pohnut = %d\n", aktualnaFigurka);
            break;
        case VYHERCA_IDENTIFIKATOR:
            //konci sa hra
            koniec = true;
            printf("Zisteny vyherca \n");
            break;
    }
}

int posliSpravu(int odosielatel, int pTypSpravy) {
    char obsahSpravy[65];
    switch (pTypSpravy) {
        case POCET_HRACOV_IDENTIFIKATOR:
            sprintf(obsahSpravy, "%d", pocetHracov);
            break;
        case HRAC_NA_RADE_IDENTIFIKATOR:
            sprintf(obsahSpravy, "%d", hracNaRade);
            break;
        case HOD_HRACA_IDENTIFIKATOR:
            sprintf(obsahSpravy, "%d", hod);
            break;
        case POHYB_FIGURKOU_IDENTIFIKATOR:
            sprintf(obsahSpravy, "%d", aktualnaFigurka);
            break;
        case KONIEC_HRY_IDENTIFIKATOR:
            sprintf(obsahSpravy, "%d", 1);
            break;
        case CHAT_IDENTIFIKATOR:
            strcpy(obsahSpravy, komentar);
            break;
        default:
            sprintf(obsahSpravy, "%d", -1);
            break;
    }
    if (odosielatel == SERVER) {
        for (int i = 0; i < pocetHracov - 1; i++) {
            if ((hracNaRade == 0) || (pTypSpravy == HRAC_NA_RADE_IDENTIFIKATOR) || (i + 1) != hracNaRade || pTypSpravy == KONIEC_HRY_IDENTIFIKATOR || pTypSpravy == CHAT_IDENTIFIKATOR) {

                char typSpravy[70];
                sprintf(typSpravy, "%d", pTypSpravy);
                strcat(typSpravy, " ");

                strcat(typSpravy, obsahSpravy);
                //potom sa prerobi na nove vlakna kde bude oddelovac $
                strcat(typSpravy, "$");

                const char *msg = typSpravy;
                //n = write(hraci[i + 1].socketHraca, msg, strlen(msg) + 1);
                n = write(hraci[i + 1].socketHraca, msg, strlen(msg));
                if (n < 0) {
                    perror("Error writing to socket");
                    return 5;
                }
            }
        }
    } else {
        char typSpravy[70];
        sprintf(typSpravy, "%d", pTypSpravy);
        strcat(typSpravy, " ");
        strcat(typSpravy, obsahSpravy);

        //prerobene pre nove vlakna s oddelovacom sprav pomocou $
        strcat(typSpravy, "$");

        const char *msg = typSpravy;
        //prerobene pre nove vlakna s oddelovacom sprav pomocou $
        n = write(sockfd, msg, strlen(msg));
        //n = write(sockfd, msg, strlen(msg) + 1);
        if (n < 0) {
            perror("Error writing to socket");
            return 5;
        }
    }
    return 1;
}