#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>
#include <pthread.h>

#define SERVER 0
#define KLIENT 1
#define MAX_POCET_KLIENTOV 3
#define MAX_POCET_HRACOV 4

#define POCET_HRACOV_IDENTIFIKATOR 1
#define HRAC_NA_RADE_IDENTIFIKATOR 2
#define HOD_HRACA_IDENTIFIKATOR 3
#define POHYB_FIGURKOU_IDENTIFIKATOR 4
#define VYHERCA_IDENTIFIKATOR 5
#define CHAT_IDENTIFIKATOR 6
#define KONIEC_HRY_IDENTIFIKATOR 7
#define NEDEFINOVANE -1

#define INDEX_ZLTY_HRAC 0
#define INDEX_CERVENY_HRAC 10
#define INDEX_MODRY_HRAC 20
#define INDEX_ZELENY_HRAC 30

#define INDEX_DOMCEK_ZLTY_HRAC 0
#define INDEX_DOMCEK_CERVENY_HRAC 4
#define INDEX_DOMCEK_MODRY_HRAC 8
#define INDEX_DOMCEK_ZELENY_HRAC 12

#define INDEX_ZACIATOK_ZLTY_HRAC 0
#define INDEX_ZACIATOK_CERVENY_HRAC 4
#define INDEX_ZACIATOK_MODRY_HRAC 8
#define INDEX_ZACIATOK_ZELENY_HRAC 12

#define HRAC_NEHRA -2
#define POCET_FIGUROK 4
#define POCET_POLI 40

#define OBSADENE_POLE 1
#define PRAZDNE_POLE -1

#define NA_ZACIATOCNOM_POLI -1
#define HRAC_INFO 0
#define FIGURKA_INFO 1

#define NEMOZNY_POHYB -1

bool server = false;
char prijateSpravy[10][256];

int pocetHracov = 1;
int n;
char buffer[256];
int sockfd, newsockfd;
socklen_t cli_len;
struct sockaddr_in serv_addr, cli_addr;

bool koniec = false;
int volba = -1;
int hod = -1;
int aktualnaFigurka = -1;
int hracNaRade = -1;
int hraciePolia[40][2];
int finalnePolia[16];
int zaciatocnePolia[16];
char komentar[65];

void precitajSpravu(int socket);

int posliSpravu(int odosielatel, int pTypSpravy);

int hodKockou();

void inicializaciaIndexovPolicok();

const char *dajFarbuHraca(int indexHraca);

int zistiPotencialnyTah(int figurka);

bool maAktualnyHracVsetkychPanacikovNaZaciatku();

int pohniFigurkou(int figurka);

void vyhodPanacika(int hracVyhadzovanehoPanacika, int vyhadzovanyPanacik);

//bool jeMoznyPohybNejakouFigurkou();

void vykresleniePolicok();

int zistiVitaza();

void posliKomentar(int oznacenieHraca);

void vymazPole(char *pole, int velkost);

void *spustenieHry(void *args);

void *skusPripojit(void *args);

int vykonaj(char *pSprava);

void *hraj(void *args);

void *vykonajSpravu(void *args);

void *citajSpravy(void *args);

void *klientHra(void *args);

typedef struct hrac {
    //ak je hrac server tak bude mat v sockete -1
    int socketHraca;
    int indexyPolicok[4];
    pthread_mutex_t *mutexClovece;
} HRAC;

typedef struct spustanie {
    bool *spustenieHry;
    pthread_mutex_t *mutexPripajanie;
} SPUSTANIE;

typedef struct pripajanie {
    bool *spustenieHry;
    pthread_mutex_t *mutexPripajanie;
} PRIPAJANIE;

typedef struct citacSprav {
    //volatile char *zoznamSprav;
    volatile int *pocetNacitanych;
    int socket;
    pthread_mutex_t *mutexClovece;
    pthread_cond_t *condNacitajSpravu;
    pthread_cond_t *condCitajPrijatuSpravu;
} CIT_SPRAV;

typedef struct vykonavacSprav {
    //volatile char *zoznamSprav;
    volatile int *pocetNacitanych;
    //int socket;
    pthread_mutex_t *mutexClovece;
    pthread_cond_t *condNacitajSpravu;
    pthread_cond_t *condCitajPrijatuSpravu;
    pthread_cond_t *condVykonajSpravu;
    pthread_mutex_t *mutexHra;
    pthread_cond_t *condHraj;
} VYK_SPRAV;

typedef struct hra {
    pthread_mutex_t *mutexHra;
    pthread_cond_t *condVykonajSpravu;
    pthread_cond_t *condHraj;
    pthread_cond_t *condCitajPrijatuSpravu;
} HRA;

typedef struct hraKlient {
    pthread_mutex_t *mutexHra;
    pthread_cond_t *condVykonajSpravu;
    pthread_cond_t *condHraj;
    pthread_cond_t *condCitajPrijatuSpravu;
    int oznacenieHraca;
} HRA_KLIENT;

HRAC hraci[MAX_POCET_HRACOV];
